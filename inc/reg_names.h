
#ifndef __REG_NAMES__
#define __REG_NAMES__

#define __In    volatile const
#define __Out   volatile
#define __InOut volatile

typedef struct {
  __InOut unsigned int long PINADDR[255];
  __InOut unsigned int long DATA;
  __InOut unsigned int long DIR;
  __InOut unsigned int long IS;
  __InOut unsigned int long IBE;
  __InOut unsigned int long IEV;
  __InOut unsigned int long IM;
  __In    unsigned int long RIS;
  __In    unsigned int long MIS;
  __Out   unsigned int long ICR;
  __InOut unsigned int long AFSEL;
  __In    unsigned int long PAD1[(0x500-0x420)/4 - 1];
  __InOut unsigned int long DR2R;
  __InOut unsigned int long DR4R;
  __InOut unsigned int long DR8R;
  __InOut unsigned int long ODR;
  __InOut unsigned int long PUR;
  __InOut unsigned int long PDR;
  __InOut unsigned int long SLR;
  __InOut unsigned int long DEN;
  __InOut unsigned int long LOCK;
  __In    unsigned int long CR;
  __InOut unsigned int long AMSEL;
  __InOut unsigned int long PCTL;
  __InOut unsigned int long ADCCTL;
  __InOut unsigned int long DMACTL;
  __In    unsigned int long PAD2[(0xFD0-0x534)/4 - 1];
  __In    unsigned int long PID4;
  __In    unsigned int long PID5;
  __In    unsigned int long PID6;
  __In    unsigned int long PID7;
  __In    unsigned int long PID0;
  __In    unsigned int long PID1;
  __In    unsigned int long PID2;
  __In    unsigned int long PID3;
  __In    unsigned int long PCID0;
  __In    unsigned int long PCID1;
  __In    unsigned int long PCID2;
  __In    unsigned int long PCID3;
} __GPIO;

// Base addresses
#define BASE_PORT_A (0x40004000UL)
#define BASE_PORT_B (0x40005000UL)
#define BASE_PORT_C (0x40006000UL)
#define BASE_PORT_D (0x40007000UL)
#define BASE_PORT_E (0x40024000UL)
#define BASE_PORT_F (0x40025000UL)

// Assign the Ports
#define TREAT_AS_GPIO_PORT_A(x) __GPIO *x = (__GPIO *)BASE_PORT_A
#define TREAT_AS_GPIO_PORT_B(x) __GPIO *x = (__GPIO *)BASE_PORT_B
#define TREAT_AS_GPIO_PORT_C(x) __GPIO *x = (__GPIO *)BASE_PORT_C
#define TREAT_AS_GPIO_PORT_D(x) __GPIO *x = (__GPIO *)BASE_PORT_D
#define TREAT_AS_GPIO_PORT_E(x) __GPIO *x = (__GPIO *)BASE_PORT_E
#define TREAT_AS_GPIO_PORT_F(x) __GPIO *x = (__GPIO *)BASE_PORT_F

#define RCGC2 (*((volatile unsigned long *)0x400FE108UL))

#endif
