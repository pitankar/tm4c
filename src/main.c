#include "reg_names.h"


int main( void ){
  TREAT_AS_GPIO_PORT_F(GPIO_PORT_F);

  RCGC2      |= 0x00000020;
  GPIO_PORT_F->DIR   = 0x0E;
  GPIO_PORT_F->PUR   = 0x11;
  GPIO_PORT_F->DEN   = 0x1F;

  while (1) {
    for (int i = 0; i < 500000; i++);
    GPIO_PORT_F->PINADDR[2]  = ~GPIO_PORT_F->PINADDR[2];
  }
}
